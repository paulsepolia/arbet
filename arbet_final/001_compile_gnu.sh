#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=gnu++14       \
       arbet.cpp          \
       driver_program.cpp \
       -lboost_system     \
       -lboost_filesystem \
       -o x_gnu
