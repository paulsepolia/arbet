
//================//
// Driver program //
//================//

//========================================================//
// Note: Merges many static libraries into a new one.     //
//       Same objects from each static library are        //
//       NOT overwritten by each other during extraction. //
//========================================================//

//=======//
// arbet //
//=======//

#include "arbet.h"

using pgg::arbet;

//=====//
// C++ //
//=====//

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

//=======//
// BOOST //
//=======//

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

// the main function

int main()
{
	//============//
	// STEP --> 1 //
	//============//

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/finished/arbet";

	//============//
	// STEP --> 2 //
	//============//

     // new library

	// Here I declare the name and directory of the new library
	// the program will create

     const string lib_new_dir     = base_dir + "/not_monitored/lib_new";
     const string lib_new_name    = "libmkl_all.a";

	//============//
	// STEP --> 3 //
	//============//

     // libraries to be merged

	// Here I declare the names and locations of the existing libraries.
	// I want to merge them to create the declared above one.
	// The objects_*_dir are tmp directories created by the program
	// where the objects are being extracted.
	// On exit the extracted objects are deleted.

	// In this example I merge the three MKL static libraries:
	// libmkl_core.a
	// libmkl_intel_thread.a
	// libmkl_intel_lp64.a
	// and I create the new one libmkl_all.a
	// This way I avoid the use of a linkage like the following:

	//  -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a 
     //  $(MKLROOT)/lib/intel64/libmkl_core.a 
     //  $(MKLROOT)/lib/intel64/libmkl_intel_thread.a -Wl,--end-group

	// The linkage has been simplified into the much simpler
	// $(MKL_NEW_PATH)/libmkl_all.a
	

     const string objects_1_dir = base_dir + "/not_monitored/lib_objects_1_tmp";
     const string lib_1_dir     = base_dir + "/not_monitored/libs_original"; // must exists
     const string lib_1_name    = "libmkl_core.a";  // must exists

     const string objects_2_dir = base_dir + "/not_monitored/lib_objects_2_tmp";
     const string lib_2_dir     = base_dir + "/not_monitored/libs_original"; // must exists
     const string lib_2_name    = "libmkl_intel_thread.a"; // must exists

     const string objects_3_dir = base_dir + "/not_monitored/lib_objects_3_tmp";
     const string lib_3_dir     = base_dir + "/not_monitored/libs_original"; // must exists
     const string lib_3_name    = "libmkl_intel_lp64.a"; // must exists

	//============//
	// STEP --> 4 //
	//============//

     // build the input vector
	// adjust according to the above input

     vector<string> vec_input;

     vec_input.push_back(lib_1_dir);
     vec_input.push_back(lib_1_name);
     vec_input.push_back(objects_1_dir);

     vec_input.push_back(lib_2_dir);
     vec_input.push_back(lib_2_name);
     vec_input.push_back(objects_2_dir);

     vec_input.push_back(lib_3_dir);
     vec_input.push_back(lib_3_name);
     vec_input.push_back(objects_3_dir);

     // call the function

     arbet arbetObj;

     arbetObj.arbet_merge_many_dp_free_fast(vec_input,
                                            lib_new_dir,
                                            lib_new_name);

     // exit

     return 0;
}

// END

