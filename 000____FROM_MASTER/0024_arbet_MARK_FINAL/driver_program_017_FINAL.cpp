
//====================================//
// driver program for the arbet class //
//====================================//

// Note: Merges many static libraries into a new one
//       same objects from each static library are
//       NOT overwritten by each other during extraction.

#include "arbet.h"

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir      = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // library to play with

     const string objects_1_dir = base_dir + "/not_monitored/lib_objects_1";
     const string lib_1_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_1_name    = "libmkl_core.a";

     arbet arbetObj;

     const auto I_MAX = 1000;

     for (int i = 0; i != I_MAX; ++i) {
          cout << "----------------------------------------------------->> " << i << endl;

          arbetObj.arbet_remove_duplicates_v2(lib_1_dir,
                                              lib_1_name,
                                              objects_1_dir);
     }

     // exit

     return 0;
}

//======//
// FINI //
//======//

