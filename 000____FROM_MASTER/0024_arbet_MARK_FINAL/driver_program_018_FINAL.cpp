
//====================================//
// driver program for the arbet class //
//====================================//

// Note: copy file

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>
#include <ctime>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;
using std::clock;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::fixed;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be read

     const string dir_name_in  = "/root/pgg_iliko/software/microsoft_products";
     const string file_name_in = "visual_studio_2013.rar";

     // file to be written

     const string dir_name_out  = base_dir + "/not_monitored/libs_original_new";
     const string file_name_out = "visual_studio_2013.rar";

     // local variables

     arbet arbetObj;

     const auto I_MAX = 100000;
     const long BUFFER = 4*1024*1024*1024L;
     time_t t1;
     time_t t2;

     cout << fixed;
     cout << setprecision(10);
     cout << showpos;
     cout << showpoint;

     for (int i = 0; i != I_MAX; ++i) {
          cout << "---------------------------------------------------------->> " << i << endl;

          t1 = clock();

          // copy the file

          arbetObj.arbet_copy_file(dir_name_in,
                                   file_name_in,
                                   dir_name_out,
                                   file_name_out,
                                   BUFFER);

          // delete file

          arbetObj.arbet_delete_files(dir_name_out, file_name_out);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

     }

     // exit

     return 0;
}

//======//
// FINI //
//======//

