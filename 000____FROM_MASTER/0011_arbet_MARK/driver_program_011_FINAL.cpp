
//====================================//
// driver program for the arbet class //
//====================================//

// Note: find and list duplicated objects

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be read

     const string lib_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_name = "libmkl_core.a";

     // file to be written

     const string lib_dir_new  = base_dir + "/not_monitored/libs_original_new";
     const string lib_name_new = "libmkl_core_new.a";

	// local variables

	arbet arbetObj;

     // produce list of duplicated objects

     arbetObj.arbet_copy_static_lib(lib_dir,
                                    lib_name,
                                    lib_dir_new,
							 lib_name_new);

     // exit

     return 0;
}

//======//
// FINI //
//======//

