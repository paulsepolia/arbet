
#ifndef ARBET_H
#define ARBET_H

//=============//
// class arbet //
//=============//

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace pgg {

class arbet {
public:

     //==================================//
     // No constructors - No destructors //
     //==================================//

     //==================//
     // member functions //
     //==================//

     // --> 1

     void arbet_extract(const string &,        // folder of the library
                        const string &,        // file name of the library
                        const string &) const; // folder of the objects to be written

     // --> 2

     void arbet_extract(const string &,        // folder of the library
                        const string &,        // file name of the library
                        const string &,        // folder of the object to be extracted
				    const string &) const; // name of the object to be extracted

     // --> 3

     void arbet_extract_create(const string &,        // folder of the original library
                               const string &,        // file name of the original library
                               const string &,        // folder of the objects to be written
                               const string &,        // folder of the new library
                               const string &) const; // file name of the new library

     // --> 4

     void arbet_merge_two(const string &,        // folder of the library A
                          const string &,        // file name of the library A
                          const string &,        // folder of the objects to be written
                          const string &,        // folder of the library B
                          const string &,        // file name of the library B
                          const string &,        // folder of the objects to be written
                          const string &,        // folder of the new library C
                          const string &) const; // name of the new library C

     // --> 5

     void arbet_extract_many(const vector<string> &) const; // vector to hold all the info for input libs

     // --> 6

     void arbet_merge_many(const vector<string> &, // vector to hpld all the info for input libs
                           const string &,         // folder of the new library C
                           const string &) const;  // name of the new library

     // --> 7

     void arbet_create_file_ar_t(const string &, 		 // folder of the library
                                 const string &, 		 // name of the library
                                 const string &, 		 // folder of the file to be written
                                 const string &) const; // name of created file ar t

     // --> 8

     void arbet_sort_file_by_line(const string &,        // folder of the file
                                  const string &,        // name of the list ot be sorted by line
                                  const string &,        // folder of the sorted file
                                  const string &) const; // name of the sorted list

     // --> 9

     void arbet_find_duplicated_lines(const string &, 	      // folder of the file
                                      const string &, 	      // name of the file
                                      const string &,        // folder of the new file
                                      const string &) const; // name of the output file

     // --> 10

     void arbet_duplicated_objects(const string &, 	   // folder of the static lib
                                   const string &, 	   // name of the static lib
                                   const string &,        // folder of the output file
                                   const string &) const; // name of the output file

     // --> 11

     bool arbet_is_file_empty(const string &,        // folder of file name
                              const string &) const; // name of file

     // --> 12

     void arbet_copy_file(const string &,        // folder of file to be copied
                          const string &,        // name of the file to be copied
                          const string &,        // folder of the file to be written
                          const string &) const; // name of the file to be written

     // --> 13

     void arbet_copy_static_lib(const string &,        // folder of the static lib
                                const string &,        // name of the lib
                                const string &,        // folder of the new static lib
                                const string &) const; // name of the new static lib

     // --> 14

     void arbet_copy_file(const string &,      // folder of file to be copied
                          const string &,      // name of the file to be copied
                          const string &,      // folder of the file to be written
                          const string &,      // name of the file to be written
                          const long &) const; // buffer size in bytes

     // --> 15

     void arbet_copy_static_lib(const string &,      // folder of the static lib
                                const string &,      // name of the lib
                                const string &,      // folder of the new static lib
                                const string &,      // name of the new static lib
                                const long &) const; // buffer size in bytes
	
	// --> 16

	void arbet_rename_file(const string &,        // folder of the file 
					   const string &,        // name of the file
					   const string &) const; // new name of the file

	// --> 17

	void arbet_mv_file(const string &,        // folder of the file
				    const string &,        // name of the file
				    const string &,        // new folder of the file
				    const string &) const; // new name of the file

	// --> 18

	void arbet_cp_file(const string &,        // folder of the file  
				    const string &,        // name of the file
				    const string &,        // new folder of the file
				    const string &) const; // new name of the file

	// --> 19

	void arbet_delete_files(const string &,        // folder of the file to be deleted
					    const string &) const; // name of the file to be deleted

	// --> 20

	void arbet_rearrange_object(const string &, 		 // libs folder
						   const string &, 		 // libs name
						   const string &) const; // object's name

	// --> 21

     void arbet_remove_duplicates(const string &,        // folder of the static lib
                                  const string &,        // name of the lib
                                  const string &,        // folder of the duplicates free lib
                                  const string &) const; // name of the duplicates free lib

private:

     // private member functions

     template <class T>
     void arbet_check_system(const T &) const;

     // commands

     const string sort_cmd        = "sort";
     const string redirection_op  = ">";
     const string uniq_cmd        = "uniq";
     const string minus_c         = "-c";
     const string minus_d         = "-d";
     const string mv_cmd          = "mv";
     const string ar_cmp          = "ar";
     const string ar_t            = "ar t";
     const string ar_x            = "ar x";
     const string ar_r            = "ar r";
	const string ar_m            = "ar m";
     const string mv_o            = "mv *.o";
     const string all_o           = "*.o";
     const string pipe            = "|";
	const string whitespace      = " ";
	const string rm_cmd          = "rm";
	const string rm_minus_rf     = "rm -rf";

     // filesystem seperators

     const string space = " ";
     const string slash = "/";
};

} // end of namespace --> pgg

//======//
// FINI //
//======//

#endif // end of ARBET_H

