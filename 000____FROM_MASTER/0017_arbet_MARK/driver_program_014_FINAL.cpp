
//====================================//
// driver program for the arbet class //
//====================================//

// Note: remove duplicated object files
//       from a static lib by rename the duplicated
//       objects and then add them back again


#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be read

     const string dir_name_in  = base_dir + "/not_monitored/libs_original";
     const string file_name_in = "libmkl_core.a";

     // file to be written

     const string dir_name_out  = base_dir + "/not_monitored/libs_new";
     const string file_name_out = "libmkl_core_free.a";

     // local variables

     arbet arbetObj;

     // remove duplicated object files

     arbetObj.arbet_remove_duplicates(dir_name_in,
                                      file_name_in,
                                      dir_name_out,
                                      file_name_out);

     // clean the object files

     arbetObj.arbet_delete_files(dir_name_out,"*.o");

     // exit

     return 0;
}

//======//
// FINI //
//======//

