
//====================================//
// driver program for the arbet class //
//====================================//

// Note: test if the a file (ascii or binary) is empty

#include "arbet.h"

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to check

     const string file_dir  = base_dir + "/not_monitored/DO_NOT_DELETE_YET";
     const string file_name = "a.out";

     // local variables

     bool tmp_bool;

     // call the function

     arbet arbetObj;

     tmp_bool = arbetObj.arbet_is_file_empty(file_dir,
                                             file_name);

     // return the result

     cout << boolalpha;

     cout << tmp_bool << endl;

     // exit

     return 0;
}

//======//
// FINI //
//======//

