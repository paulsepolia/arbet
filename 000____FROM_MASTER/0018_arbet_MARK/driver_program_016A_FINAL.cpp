
//====================================//
// driver program for the arbet class //
//====================================//

// Note: Merges many static libraries into a new one
//       same objects from each static library are
//       NOT overwritten by each other during extraction.

#include "arbet.h"

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir      = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // new library

     const string lib_new_dir     = base_dir + "/not_monitored/libs_new";
     const string lib_new_name    = "libmkl_intel_6_pgg5.a";

     // libraries to be merged

     const string objects_1_dir = base_dir + "/not_monitored/lib_objects_1";
     const string lib_1_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_1_name    = "libmkl_core.a";

     const string objects_2_dir = base_dir + "/not_monitored/lib_objects_2";
     const string lib_2_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_2_name    = "libmkl_core.a";

     const string objects_3_dir = base_dir + "/not_monitored/lib_objects_3";
     const string lib_3_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_3_name    = "libmkl_core.a";

     const string objects_4_dir = base_dir + "/not_monitored/lib_objects_4";
     const string lib_4_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_4_name    = "libmkl_core.a";

     const string objects_5_dir = base_dir + "/not_monitored/lib_objects_5";
     const string lib_5_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_5_name    = "libmkl_intel_thread.a";

     const string objects_6_dir = base_dir + "/not_monitored/lib_objects_6";
     const string lib_6_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_6_name    = "libmkl_core.a";

     // build the input vector

     vector<string> vec_input;

     vec_input.push_back(lib_1_dir);
     vec_input.push_back(lib_1_name);
     vec_input.push_back(objects_1_dir);

     vec_input.push_back(lib_2_dir);
     vec_input.push_back(lib_2_name);
     vec_input.push_back(objects_2_dir);

     vec_input.push_back(lib_3_dir);
     vec_input.push_back(lib_3_name);
     vec_input.push_back(objects_3_dir);

     vec_input.push_back(lib_4_dir);
     vec_input.push_back(lib_4_name);
     vec_input.push_back(objects_4_dir);

     vec_input.push_back(lib_5_dir);
     vec_input.push_back(lib_5_name);
     vec_input.push_back(objects_5_dir);

     vec_input.push_back(lib_6_dir);
     vec_input.push_back(lib_6_name);
     vec_input.push_back(objects_6_dir);

     // call the function

     arbet arbetObj;

     arbetObj.arbet_merge_many_dp_free(vec_input,
                               	    lib_new_dir,
                               	    lib_new_name);

     // exit

     return 0;
}

//======//
// FINI //
//======//

