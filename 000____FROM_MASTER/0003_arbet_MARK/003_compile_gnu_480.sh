#!/bin/bash

  # 1. compile

  g++-4.8  -O3                \
           -Wall              \
           -std=c++0x         \
           -static            \
           arbet.cpp          \
           driver_program.cpp \
           -I/opt/boost/1560/gnu_480                               \
           /opt/boost/1560/gnu_480/stage/lib/libboost_system.a     \
           /opt/boost/1560/gnu_480/stage/lib/libboost_filesystem.a \
           -o x_gnu_480
