
//====================================//
// driver program for the arbet class //
//====================================//

// Note: sort a file by line

#include "arbet.h"

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::string;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be sorted

     const string file_dir  = base_dir + "/not_monitored/help_files";
     const string file_name = "out_ar_t_1";
	
	// file sorted

	const string file_dir_out = base_dir + "/not_monitored/sorted_files";
	const string file_name_out = "out_ar_t_2";

     // call the function

     arbet arbetObj;

     arbetObj.arbet_sort_file_by_line(file_dir,
                                      file_name,
                                      file_dir_out,
							   file_name_out);

     // exit

     return 0;
}

//======//
// FINI //
//======//

