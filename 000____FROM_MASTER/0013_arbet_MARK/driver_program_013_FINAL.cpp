
//====================================//
// driver program for the arbet class //
//====================================//

// Note: copy file using buffers

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be read

     const string dir_name_in  = "/root/pgg_iliko/software/egkefalos_soft";
     const string file_name_in = "matlab_2014a_win.rar";

     // file to be written

     const string dir_name_out  = base_dir + "/not_monitored/libs_original_new";
     const string file_name_out = "libmkl_core_new.a";

	// buffer size

	const long buffer_size = 2*1024*1024*1024LL; 

	// local variables

	arbet arbetObj;

	// copy the file

     arbetObj.arbet_copy_file(dir_name_in,
                              file_name_in,
                              dir_name_out,
					     file_name_out,
					     buffer_size);

     // exit

     return 0;
}

//======//
// FINI //
//======//

