
#ifndef ARBET_DEF_H
#define ARBET_DEF_H

//========================//
// arbet class definition //
//========================//

#include "arbet.h"

// standard c++ library

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;

// boost c++ library

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

namespace pgg {

//=========================================//
// arbet class member functions definition //
//=========================================//

// --> 1

template <>
void arbet::arbet_check_system(const int & tmp_int) const
{
     if (tmp_int == 0) {
          cout << " --> system command executed with success" << endl;
     } else {
          cout << " --> ERROR: system command did not execute!" << endl;
          cout << " --> Enter an integer to exit:";
          int sentinel;
          cin >> sentinel;
          exit(-1);
     }
}

// --> 2

template <>
void arbet::arbet_check_system(const bool & tmp_bool) const
{
     if (tmp_bool == true) {
          cout << " --> directory created with success" << endl;
     } else {
          cout << " --> directory already exists" << endl;
     }
}


// --> 3

//=============================================//
// Extract object from a static library        //
// to spefified folder                         //
//=============================================//

//==================================================//
// Note: If there are objects with the same name    //
//       then are overwritten by the last extracted //
//========================================-=========//

void arbet::arbet_extract(const string & lib_orig_dir,
                          const string & lib_orig_name,
                          const string & objects_dir) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // create director for the object files

     tmp_bool = create_directories(objects_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // extract the objects

     tmp_string = ar_x +
                  space +
                  lib_orig_dir + slash + lib_orig_name;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

     // move the object to specific location

     tmp_string = mv_o +
                  space +
                  objects_dir;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 4

//=============================================//
// Extract object from a static library        //
// and archives them again to create a new one //
//=============================================//

//==========================================================//
// Note: The new static library must be the same            //
//       with the original one UNLESS there are             //
//       objects with the same name in the original library //
//       so during the extraction process the objects       //
//       with the same name overwrites each other           //
//       so we end up with one instance only                //
//       of those objects                                   //
//==========================================================//

void arbet::arbet_extract_create(const string & lib_orig_dir,
                                 const string & lib_orig_name,
                                 const string & objects_dir,
                                 const string & lib_new_dir,
                                 const string & lib_new_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects

     arbet_extract(lib_orig_dir,
                   lib_orig_name,
                   objects_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  objects_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

}

// --> 5

//=================================================//
// Note: extracts objects from two static libaries //
//       then builds a new one with those objects  //
//=================================================//

void arbet::arbet_merge_two(const string & lib_a_orig_dir,
                            const string & lib_a_orig_name,
                            const string & objects_a_dir,
                            const string & lib_b_orig_dir,
                            const string & lib_b_orig_name,
                            const string & objects_b_dir,
                            const string & lib_c_dir,
                            const string & lib_c_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects of the first library

     arbet_extract(lib_a_orig_dir,
                   lib_a_orig_name,
                   objects_a_dir);

     // exctract the objects of the second library

     arbet_extract(lib_b_orig_dir,
                   lib_b_orig_name,
                   objects_b_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_c_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_c_dir + slash + lib_c_name +
                  space +
                  objects_a_dir + slash + all_o +
                  space +
                  objects_b_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

}

// --> 6

//==================================================//
// Note: extracts objects from many static libaries //
//==================================================//

void arbet::arbet_extract_many(const vector<string> & vec_input) const

{
     // local variables

     int tmp_int;
     int i;

     // get vector's length

     tmp_int = vec_input.size();

     // test if it is ok

     if ( ((tmp_int >=3) && (tmp_int%3 != 0)) || (tmp_int < 3)) {
          cout << " --> ERROR: input vector's size is wrong!" << endl;
          cout << " --> Enter an integer to exit: ";
          int sentinel;
          cin >> sentinel;
     }

     // extract all the objects

     for (i = 0; i < tmp_int; i = i+3) {
          arbet_extract(vec_input[i],
                        vec_input[i+1],
                        vec_input[i+2]);
     }
}

// --> 7

//==================================================//
// Note: extracts objects from many static libaries //
//       then builds a new one with those objects   //
//==================================================//

void arbet::arbet_merge_many(const vector<string> & vec_input,
                             const string & lib_new_dir,
                             const string & lib_new_name) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     unsigned i;
     int tmp_int;

     // extract all the objects

     arbet_extract_many(vec_input);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build string with all the input arguments to ar command

     tmp_string = "";

     for (i = 0; i < vec_input.size(); i = i+3) {
          tmp_string = tmp_string +
                       vec_input[i+2] + slash + all_o +
                       space;
     }

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  tmp_string;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 8

//==================================================//
// Note: create file list with the output of        //
//       the command "ar t" applied on a static lib //
//==================================================//

void arbet::arbet_create_file_ar_t(const string & lib_dir,
                                   const string & lib_name,
                                   const string & file_dir,
 							const string & file_name) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

	// create the ar t command

	tmp_string = ar_t + 
			   space +
			   lib_dir + slash + lib_name +
			   space +
			   redirection_op +
			   space +
			   file_name;

	// execute the command

	tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);

     // create directory for the new file

     tmp_bool = create_directories(file_dir.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// move the new file to its location

     tmp_string = mv_cmd +
                  space +
                  file_name + 
			   space +
		        file_dir;

	// execute the command

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 9

//=========================//
// Note: sort file by line //
//=========================//

void arbet::arbet_sort_file_by_line(const string & file_dir,
                                    const string & file_name,
                                    const string & file_dir_out,
 							 const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// create the sort command

	tmp_string = sort_cmd + 
			   space +
			   file_dir + slash + file_name +
			   space +
			   redirection_op +
			   space +
			   file_dir_out + slash + file_name_out;

	// execute the command

	tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 10

//===============================================//
// Note: find duplicated lines in a file by line //
//===============================================//

void arbet::arbet_find_duplicated_lines(const string & file_dir,
                                        const string & file_name,
                                        const string & file_dir_out,
 							     const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// create the find duplicated lines command

	tmp_string = uniq_cmd + 
			   space +
			   minus_d +
			   space +
			   minus_c + 
			   space +
			   file_dir + slash + file_name +
			   space +
			   redirection_op +
			   space +
			   file_dir_out + slash + file_name_out;

	// execute the command

	tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 11

//===============================================//
// Note: find duplicated lines in a file by line //
//===============================================//

void arbet::arbet_duplicated_objects(const string & lib_dir,
                                     const string & lib_name,
                                     const string & file_dir_out,
 							  const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// create the duplicated objects command

	tmp_string = ar_t + 
			   space +
			   lib_dir + slash + lib_name +
			   space +
			   pipe + 
			   space +
			   sort_cmd +
			   space +
			   pipe +
			   space +
			   uniq_cmd + 
			   space +
			   minus_c +
			   space +
			   minus_d +
		        space +
			   redirection_op +
			   space +
			   file_dir_out + slash + file_name_out;

	// execute the command

	tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 12

//===================================================//
//  Note: check if a file (ascii or binary) is empty //
//===================================================//

bool arbet::arbet_is_file_empty(const string & file_dir,
	   				       const string & file_name) const
{
	// local variables
	
	ifstream in_file;
	string tmp_string;
	int tmp_int;

	// set the absolute file path

	tmp_string = file_dir + slash + file_name;

	// open ascii file to read

	in_file.open(tmp_string.c_str(), ios::in);

	// go to the end of the file

     in_file.seekg(0, in_file.end);
	
	// get the size of the file

	tmp_int = in_file.tellg();

	// close the file

	in_file.close();

	if (tmp_int == 0)
	{
		return true;
	}

	return false;
}

// --> 13

//======================//
//  Note: copy any file //
//======================//

void arbet::arbet_copy_file(const string & dir_name_in,
	   		  		   const string & file_name_in,
					   const string & dir_name_out,
					   const string & file_name_out) const
{
	// local variables

	ifstream in_file;
	ofstream out_file;
	string tmp_string;
	long tmp_long;
	char * tmp_char_ptr;
	bool tmp_bool;

	// open file

	tmp_string = dir_name_in + slash + file_name_in;

	in_file.open(tmp_string, ios::in);

	// check is file opened with success

	if(!in_file)
	{
		cout << " --> ERROR: Input file not opened." << endl;
		cout << " --> Enter an integer to exit: ";
		int sentinel;
		cin >> sentinel;
	}

	// get the size of the file

    	in_file.seekg(0, in_file.end);

    	tmp_long = in_file.tellg();

	// return to the begin

   	in_file.seekg(0, in_file.beg);

    	// allocate heap space

    	tmp_char_ptr = new char [tmp_long];

	// read data in one a block

	in_file.read(tmp_char_ptr, tmp_long);

	// close input file stream

    	in_file.close();

     // create directory for the new file

     tmp_bool = create_directories(dir_name_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// open file to write the new lib

	tmp_string = dir_name_out + slash + file_name_out;

	out_file.open(tmp_string, ios::out);

	// write to the file

	cout << " --> copying file ... please wait!" << endl;

     out_file.write(tmp_char_ptr, tmp_long);

	// flush the output stream

	out_file.flush();

	// close file stream

	out_file.close();

	// delete allocated space

    	delete[] tmp_char_ptr;

}

// --> 14

//========================//
//  Note: copy static lib //
//========================//

void arbet::arbet_copy_static_lib(const string & lib_dir,
	   		  		         const string & lib_name,
					         const string & lib_dir_new,
					         const string & lib_name_new) const
{
	arbet_copy_file(lib_dir,
				 lib_name,
				 lib_dir_new,
				 lib_name_new);
}

// --> 15

//====================================//
//  Note: copy any file using buffers //
//====================================//

void arbet::arbet_copy_file(const string & dir_name_in,
	   		  		   const string & file_name_in,
					   const string & dir_name_out,
					   const string & file_name_out,
					   const long & buffer_size) const
{
	// local variables

	ifstream in_file;
	ofstream out_file;
	string tmp_string;
	long tmp_long;
	long tmp_long_2;
	int tmp_int;
	char * tmp_char_ptr;
	bool tmp_bool;

	// open file

	tmp_string = dir_name_in + slash + file_name_in;

	in_file.open(tmp_string, ios::in);

	// check is file opened with success

	if(!in_file)
	{
		cout << " --> ERROR: Input file not opened." << endl;
		cout << " --> Enter an integer to exit: ";
		int sentinel;
		cin >> sentinel;
	}

	// get the size of the file

    	in_file.seekg(0, in_file.end);

    	tmp_long = in_file.tellg();

	// return to the begin

   	in_file.seekg(0, in_file.beg);

     // create directory for the new file

     tmp_bool = create_directories(dir_name_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

	// open file to write the new lib

	tmp_string = dir_name_out + slash + file_name_out;

	out_file.open(tmp_string, ios::out);

	// split in buffers

	if (tmp_long <= buffer_size) // do not split in buffers
	{
    		// allocate heap space

    		tmp_char_ptr = new char [tmp_long];

		// read data in one a block

		in_file.read(tmp_char_ptr, tmp_long);

		// write to the file

		cout << " --> copying file ... please wait!" << endl;

     	out_file.write(tmp_char_ptr, tmp_long);

		// flush the output stream

		out_file.flush();

		// delete allocated space

    		delete[] tmp_char_ptr;
	}
	else
	{
		// set variables 

		tmp_int = tmp_long / buffer_size; // how many times to read one giga of data
		tmp_long_2 = tmp_long%buffer_size; // the offset

    		// allocate heap space

    		tmp_char_ptr = new char [buffer_size];

		// read and write data in a block of one giga size

		cout << " --> copying file ... please wait!" << endl;

		for (int i = 0; i != tmp_int; ++i)
		{
			in_file.read(tmp_char_ptr, buffer_size);
     		out_file.write(tmp_char_ptr, buffer_size);
			out_file.flush();
		}

		// read the final part of data

          in_file.read(tmp_char_ptr, tmp_long_2);
          out_file.write(tmp_char_ptr, tmp_long_2);
          out_file.flush();

		// delete allocated space

    		delete[] tmp_char_ptr;
	}

	// close input and out file streams

    	in_file.close();
	out_file.close();
}

// --> 16

//=====================================//
//  Note: copy static lib with buffers //
//=====================================//

void arbet::arbet_copy_static_lib(const string & lib_dir,
	   		  		         const string & lib_name,
					         const string & lib_dir_new,
					         const string & lib_name_new,
						    const long & buffer_size) const
{
	arbet_copy_file(lib_dir,
				 lib_name,
				 lib_dir_new,
				 lib_name_new,
				 buffer_size);
}

} // end of namespace --> pgg

//======//
// FINI //
//======//

#endif // ARBET_DEF_H

