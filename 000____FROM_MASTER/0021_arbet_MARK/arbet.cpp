
#ifndef ARBET_DEF_H
#define ARBET_DEF_H

//========================//
// arbet class definition //
//========================//

#include "arbet.h"

// standard c++ library

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::vector;
using std::to_string;

// boost c++ library

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

namespace pgg {

//=========================================//
// arbet class member functions definition //
//=========================================//

// --> A

template <>
void arbet::arbet_check_system(const int & tmp_int) const
{
     if (tmp_int == 0) {
          cout << " --> system command executed with success" << endl;
     } else {
          cout << " --> ERROR: system command did not execute!" << endl;
          cout << " --> Enter an integer to exit:";
          int sentinel;
          cin >> sentinel;
          exit(-1);
     }
}

// --> B

template <>
void arbet::arbet_check_system(const bool & tmp_bool) const
{
     if (tmp_bool == true) {
          cout << " --> directory created with success" << endl;
     } else {
          cout << " --> directory already exists" << endl;
     }
}


// --> 1

//=======================================//
// Extract objects from a static library //
// to spefified folder                   //
//=======================================//

//==================================================//
// Note: If there are objects with the same name    //
//       then are overwritten by the last extracted //
//========================================-=========//

void arbet::arbet_extract(const string & lib_orig_dir,
                          const string & lib_orig_name,
                          const string & objects_dir) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // create director for the object files

     tmp_bool = create_directories(objects_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // extract the objects

     tmp_string = ar_x +
                  space +
                  lib_orig_dir + slash + lib_orig_name;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

     // move the object to specific location

     tmp_string = mv_o +
                  space +
                  objects_dir;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 2

//===============================================//
// Extract spefific object from a static library //
// to spefified folder                           //
//===============================================//

void arbet::arbet_extract(const string & lib_orig_dir,
                          const string & lib_orig_name,
                          const string & object_dir,
                          const string & object_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // create director for the object files

     tmp_bool = create_directories(object_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // extract the object

     tmp_string = ar_x +
                  space +
                  lib_orig_dir + slash + lib_orig_name +
                  space +
                  object_name;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

     // move the object to specific location

     tmp_string = mv_o +
                  space +
                  object_dir;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 3

//=============================================//
// Extract object from a static library        //
// and archives them again to create a new one //
//=============================================//

//==========================================================//
// Note: The new static library must be the same            //
//       with the original one UNLESS there are             //
//       objects with the same name in the original library //
//       so during the extraction process the objects       //
//       with the same name overwrites each other           //
//       so we end up with one instance only                //
//       of those objects                                   //
//==========================================================//

void arbet::arbet_extract_create(const string & lib_orig_dir,
                                 const string & lib_orig_name,
                                 const string & objects_dir,
                                 const string & lib_new_dir,
                                 const string & lib_new_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects

     arbet_extract(lib_orig_dir,
                   lib_orig_name,
                   objects_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  objects_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

}

// --> 4

//=================================================//
// Note: extracts objects from two static libaries //
//       then builds a new one with those objects  //
//=================================================//

void arbet::arbet_merge_two(const string & lib_a_orig_dir,
                            const string & lib_a_orig_name,
                            const string & objects_a_dir,
                            const string & lib_b_orig_dir,
                            const string & lib_b_orig_name,
                            const string & objects_b_dir,
                            const string & lib_c_dir,
                            const string & lib_c_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects of the first library

     arbet_extract(lib_a_orig_dir,
                   lib_a_orig_name,
                   objects_a_dir);

     // exctract the objects of the second library

     arbet_extract(lib_b_orig_dir,
                   lib_b_orig_name,
                   objects_b_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_c_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_c_dir + slash + lib_c_name +
                  space +
                  objects_a_dir + slash + all_o +
                  space +
                  objects_b_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

}

// --> 5

//==================================================//
// Note: extracts objects from many static libaries //
//==================================================//

void arbet::arbet_extract_many(const vector<string> & vec_input) const

{
     // local variables

     int tmp_int;
     int i;

     // get vector's length

     tmp_int = vec_input.size();

     // test if it is ok

     if ( ((tmp_int >=3) && (tmp_int%3 != 0)) || (tmp_int < 3)) {
          cout << " --> ERROR: input vector's size is wrong!" << endl;
          cout << " --> Enter an integer to exit: ";
          int sentinel;
          cin >> sentinel;
     }

     // extract all the objects

     for (i = 0; i < tmp_int; i = i+3) {
          arbet_extract(vec_input[i],
                        vec_input[i+1],
                        vec_input[i+2]);
     }
}

// --> 6

//==================================================//
// Note: extracts objects from many static libaries //
//       then builds a new one with those objects   //
//==================================================//

void arbet::arbet_merge_many(const vector<string> & vec_input,
                             const string & lib_new_dir,
                             const string & lib_new_name) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     unsigned i;
     int tmp_int;

     // extract all the objects

     arbet_extract_many(vec_input);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     arbet_check_system(tmp_bool);

     // build string with all the input arguments to ar command

     tmp_string = "";

     for (i = 0; i < vec_input.size(); i = i+3) {
          tmp_string = tmp_string +
                       vec_input[i+2] + slash + all_o +
                       space;
     }

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  tmp_string;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);

	// remove objects files

     for (i = 0; i < vec_input.size(); i = i+3) {
     
		arbet_delete_files(vec_input[i+2], all_o);
	}
}

// --> 7

//==================================================//
// Note: create file list with the output of        //
//       the command "ar t" applied on a static lib //
//==================================================//

void arbet::arbet_create_file_ar_t(const string & lib_dir,
                                   const string & lib_name,
                                   const string & file_dir,
                                   const string & file_name) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create the ar t command

     tmp_string = ar_t +
                  space +
                  lib_dir + slash + lib_name +
                  space +
                  redirection_op +
                  space +
                  file_name;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);

     // create directory for the new file

     tmp_bool = create_directories(file_dir.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // move the new file to its location

     tmp_string = mv_cmd +
                  space +
                  file_name +
                  space +
                  file_dir;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 8

//=========================//
// Note: sort file by line //
//=========================//

void arbet::arbet_sort_file_by_line(const string & file_dir,
                                    const string & file_name,
                                    const string & file_dir_out,
                                    const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // create the sort command

     tmp_string = sort_cmd +
                  space +
                  file_dir + slash + file_name +
                  space +
                  redirection_op +
                  space +
                  file_dir_out + slash + file_name_out;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 9

//===============================================//
// Note: find duplicated lines in a file by line //
//===============================================//

void arbet::arbet_find_duplicated_lines(const string & file_dir,
                                        const string & file_name,
                                        const string & file_dir_out,
                                        const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // create the find duplicated lines command

     tmp_string = uniq_cmd +
                  space +
                  minus_d +
                  space +
                  minus_c +
                  space +
                  file_dir + slash + file_name +
                  space +
                  redirection_op +
                  space +
                  file_dir_out + slash + file_name_out;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 10

//===============================================//
// Note: find duplicated lines in a file by line //
//===============================================//

void arbet::arbet_duplicated_objects(const string & lib_dir,
                                     const string & lib_name,
                                     const string & file_dir_out,
                                     const string & file_name_out) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create directory for the new file

     tmp_bool = create_directories(file_dir_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // create the duplicated objects command

     tmp_string = ar_t +
                  space +
                  lib_dir + slash + lib_name +
                  space +
                  pipe +
                  space +
                  sort_cmd +
                  space +
                  pipe +
                  space +
                  uniq_cmd +
                  space +
                  minus_c +
                  space +
                  minus_d +
                  space +
                  redirection_op +
                  space +
                  file_dir_out + slash + file_name_out;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if everything is ok

     arbet_check_system(tmp_int);
}

// --> 11

//==================================================//
// Note: check if a file (ascii or binary) is empty //
//==================================================//

bool arbet::arbet_is_file_empty(const string & file_dir,
                                const string & file_name) const
{
     // local variables

     ifstream in_file;
     string tmp_string;
     long tmp_long;

     // set the absolute file path

     tmp_string = file_dir + slash + file_name;

     // open ascii file to read

     in_file.open(tmp_string.c_str(), ios::in);

     // go to the end of the file

     in_file.seekg(0, in_file.end);

     // get the size of the file

     tmp_long = in_file.tellg();

     // close the file

     in_file.close();

     if (tmp_long == static_cast<long>(0)) {
          return true;
     }

     return false;
}

// --> 12

//=====================//
// Note: copy any file //
//=====================//

void arbet::arbet_copy_file(const string & dir_name_in,
                            const string & file_name_in,
                            const string & dir_name_out,
                            const string & file_name_out) const
{
     // local variables

     ifstream in_file;
     ofstream out_file;
     string tmp_string;
     long tmp_long;
     char * tmp_char_ptr;
     bool tmp_bool;

     // open file

     tmp_string = dir_name_in + slash + file_name_in;

     in_file.open(tmp_string, ios::in);

     // check is file opened with success

     if(!in_file) {
          cout << " --> ERROR: Input file not opened." << endl;
          cout << " --> Enter an integer to exit: ";
          int sentinel;
          cin >> sentinel;
     }

     // get the size of the file

     in_file.seekg(0, in_file.end);

     tmp_long = in_file.tellg();

     // return to the begin

     in_file.seekg(0, in_file.beg);

     // allocate heap space

     tmp_char_ptr = new char [tmp_long];

     // read data in one a block

     in_file.read(tmp_char_ptr, tmp_long);

     // close input file stream

     in_file.close();

     // create directory for the new file

     tmp_bool = create_directories(dir_name_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // open file to write the new lib

     tmp_string = dir_name_out + slash + file_name_out;

     out_file.open(tmp_string, ios::out);

     // write to the file

     cout << " --> copying file ... please wait!" << endl;

     out_file.write(tmp_char_ptr, tmp_long);

     // flush the output stream

     out_file.flush();

     // close file stream

     out_file.close();

     // delete allocated space

     delete[] tmp_char_ptr;

}

// --> 13

//=======================//
// Note: copy static lib //
//=======================//

void arbet::arbet_copy_static_lib(const string & lib_dir,
                                  const string & lib_name,
                                  const string & lib_dir_new,
                                  const string & lib_name_new) const
{
     arbet_copy_file(lib_dir,
                     lib_name,
                     lib_dir_new,
                     lib_name_new);
}

// --> 14

//===================================//
// Note: copy any file using buffers //
//===================================//

void arbet::arbet_copy_file(const string & dir_name_in,
                            const string & file_name_in,
                            const string & dir_name_out,
                            const string & file_name_out,
                            const long & buffer_size) const
{
     // local variables

     ifstream in_file;
     ofstream out_file;
     string tmp_string;
     long tmp_long;
     long tmp_long_2;
     int tmp_int;
     char * tmp_char_ptr;
     bool tmp_bool;

     // open file

     tmp_string = dir_name_in + slash + file_name_in;

     in_file.open(tmp_string, ios::in);

     // check is file opened with success

     if(!in_file) {
          cout << " --> ERROR: Input file not opened." << endl;
          cout << " --> Enter an integer to exit: ";
          int sentinel;
          cin >> sentinel;
     }

     // get the size of the file

     in_file.seekg(0, in_file.end);

     tmp_long = in_file.tellg();

     // return to the begin

     in_file.seekg(0, in_file.beg);

     // create directory for the new file

     tmp_bool = create_directories(dir_name_out.c_str());

     // check if everything is ok

     arbet_check_system(tmp_bool);

     // open file to write the new lib

     tmp_string = dir_name_out + slash + file_name_out;

     out_file.open(tmp_string, ios::out);

     // split in buffers

     if (tmp_long <= buffer_size) { // do not split in buffers
          // allocate heap space

          tmp_char_ptr = new char [tmp_long];

          // read data in one a block

          in_file.read(tmp_char_ptr, tmp_long);

          // write to the file

          cout << " --> copying file ... please wait!" << endl;

          out_file.write(tmp_char_ptr, tmp_long);

          // flush the output stream

          out_file.flush();

          // delete allocated space

          delete[] tmp_char_ptr;
     } else {
          // set variables

          tmp_int = tmp_long / buffer_size; // how many times to read one giga of data
          tmp_long_2 = tmp_long%buffer_size; // the offset

          // allocate heap space

          tmp_char_ptr = new char [buffer_size];

          // read and write data in a block of one giga size

          cout << " --> copying file ... please wait!" << endl;

          for (int i = 0; i != tmp_int; ++i) {
               in_file.read(tmp_char_ptr, buffer_size);
               out_file.write(tmp_char_ptr, buffer_size);
               out_file.flush();
          }

          // read the final part of data

          in_file.read(tmp_char_ptr, tmp_long_2);
          out_file.write(tmp_char_ptr, tmp_long_2);
          out_file.flush();

          // delete allocated space

          delete[] tmp_char_ptr;
     }

     // close input and out file streams

     in_file.close();
     out_file.close();
}

// --> 15

//====================================//
// Note: copy static lib with buffers //
//====================================//

void arbet::arbet_copy_static_lib(const string & lib_dir,
                                  const string & lib_name,
                                  const string & lib_dir_new,
                                  const string & lib_name_new,
                                  const long & buffer_size) const
{
     arbet_copy_file(lib_dir,
                     lib_name,
                     lib_dir_new,
                     lib_name_new,
                     buffer_size);
}

// --> 16

//===================//
// Note: rename file //
//===================//

void arbet::arbet_rename_file(const string & file_dir,
                              const string & file_name,
                              const string & file_name_new) const
{
     // local variables

     string tmp_string;
     int tmp_int;

     // build the command

     tmp_string = mv_cmd +
                  space +
                  file_dir + slash + file_name +
                  space +
                  file_dir + slash + file_name_new;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if system is okay

     arbet_check_system(tmp_int);
}

// --> 17

//===============//
// Note: mv file //
//===============//

void arbet::arbet_mv_file(const string & file_dir,
                          const string & file_name,
                          const string & file_dir_new,
                          const string & file_name_new) const
{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create new directory

     tmp_bool = create_directories(file_dir_new.c_str());

     // check if system is okay

     arbet_check_system(tmp_bool);

     // build the command

     tmp_string = mv_cmd +
                  space +
                  file_dir + slash + file_name +
                  space +
                  file_dir_new + slash + file_name_new;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if system is okay

     arbet_check_system(tmp_int);
}

// --> 18

//===============//
// Note: cp file //
//===============//

void arbet::arbet_cp_file(const string & file_dir,
                          const string & file_name,
                          const string & file_dir_new,
                          const string & file_name_new) const
{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int tmp_int;

     // create new directory

     tmp_bool = create_directories(file_dir_new.c_str());

     // check if system is okay

     arbet_check_system(tmp_bool);

     // build the command

     tmp_string = cp_cmd +
                  space +
                  file_dir + slash + file_name +
                  space +
                  file_dir_new + slash + file_name_new;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if system is okay

     arbet_check_system(tmp_int);
}

// --> 19

//====================//
// Note: delete files //
//====================//

void arbet::arbet_delete_files(const string & dir_name,
                               const string & file_name) const
{
     // local variables

     string tmp_string;
     int tmp_int;

     // build the command

     tmp_string = rm_cmd +
                  space +
                  dir_name + slash + file_name;

     // execute the command

     tmp_int = system(tmp_string.c_str());

     // check if system is okay

	tmp_int = 0; // force to be okay

     arbet_check_system(tmp_int);
}

// --> 20

//===============================================//
// Extract spefific object from a static library //
// to spefified folder                           //
//===============================================//

void arbet::arbet_rearrange_object(const string & lib_orig_dir,
                                   const string & lib_orig_name,
                                   const string & object_name) const
{
     // local variables

     int tmp_int;
     string tmp_string;

     // rearrange object

     tmp_string = ar_m +
                  space +
                  lib_orig_dir + slash + lib_orig_name +
                  space +
                  object_name;

     tmp_int = system(tmp_string.c_str());

     // check

     arbet_check_system(tmp_int);
}

// --> 21

//===========================================//
// Note: Remove duplicates from a static lib //
//       and produce a new static lib        //
//===========================================//

void arbet::arbet_remove_duplicates(const string & lib_dir,
                                    const string & lib_name,
                                    const string & lib_dir_new,
                                    const string & lib_name_new) const
{
     // local variables

     ifstream input_stream;
     string tmp_string;
     int tmp_int;
     bool tmp_bool;
     vector<int> vec_int;
     vector<string> vec_string;

     // tmp local variables

     const string ar_t_dir = lib_dir_new;
     const string ar_t_file = "__tmp_ar_t_" + to_string(rand());

     // create the file with the list of all duplicates

     arbet_duplicated_objects(lib_dir,
                              lib_name,
                              ar_t_dir,
                              ar_t_file);

     // check if the file is empty or not

     tmp_bool = arbet_is_file_empty(ar_t_dir,
                                    ar_t_file);

     // if the file is not empty then
     // has duplicates
     // so, process the duplicates now

	// big-if starts

     if (tmp_bool == true) {

          cout << " --> Nothing to be done. Static library is duplicates free!" << endl;

		arbet_copy_file(lib_dir,
                          lib_name,
                          lib_dir_new,
                          lib_name_new);

		// delete the at_t tmp file

          arbet_delete_files(ar_t_dir, ar_t_file);

     } else if (tmp_bool == false) {
          // open stream file to read the "at t" ascii file

          tmp_string = ar_t_dir + slash + ar_t_file;

          input_stream.open(tmp_string.c_str());

          // read whole file line-by-line
          // and fill-up the containers

          while(input_stream.good()) {
               // read the integer

               input_stream >> tmp_int;

               vec_int.push_back(tmp_int);

               // read line

               getline(input_stream, tmp_string);

               vec_string.push_back(tmp_string);
          }

          // get strings vector size

          tmp_int = vec_string.size();

          // produce proper file names
		// be trimming the leading white space
		// trim string

          for (int i = 0; i != tmp_int; ++i) {
               const auto str_begin = vec_string[i].find_first_not_of(whitespace);
               const auto str_end = vec_string[i].find_last_not_of(whitespace);
               const auto str_range = str_end - str_begin + 1;

               vec_string[i] = vec_string[i].substr(str_begin, str_range); // trim the string
          }

          // extract all duplicated objects from the lib
          // and rename them

          tmp_int = vec_int.size();

          for (int j = 0; j != tmp_int; ++j) {
               for (int i = 0; i != vec_int[j]; ++i) {

                    arbet_extract(lib_dir,
                                  lib_name,
                                  ar_t_dir,
                                  vec_string[j]);

                    arbet_rename_file(ar_t_dir,
                                      vec_string[j],
                                      "pgg_" + to_string(i) + "_" + vec_string[j]);

                    arbet_rearrange_object(lib_dir,
                                           lib_name,
                                           vec_string[j]);
               }
          }

     	// extract all the objects of the lib
     	// and create the free duplicates lib

     	arbet_extract_create(lib_dir,       // folder of the original library
          	                lib_name,      // file name of the original library
               	           ar_t_dir,      // folder of the objects to be written
                    	      lib_dir_new,   // folder of the new library
                         	 lib_name_new); // file name of the new library

		// delete all the objects files

         	arbet_delete_files(ar_t_dir, all_o);

		// delete the at_t tmp file

		arbet_delete_files(ar_t_dir, ar_t_file);

	} // end of big-if

}

// --> 22 --> NOT THE BEST VERSION

//================================//
// Note: arbet_merge_many_dp_free //
//================================//

// merges many static libraries creating a new one
// but there are no duplicates
// the algorithm i use is as follows
// 1 --> I detect if the libraries have duplicates
// 2 --> if yes, then I remove the duplicates and build the lib again (BAD)
// 3 --> if not, then i just copy the lib into the proper dir
// 4 --> i extract all the oblects to their own local folders
// 5 --> I merge to build a new one
// 6 --> since the objects are being extracted to theor own local dir
//   --> there are no overwrites
// 7 --> even if there are objects with same name coming from 
//   --> different libs, during the final merge there is no overwrite
//   --> since that is the way "ar" command works
// 8 --> the step 2 needs improvements, so as to avoid to build the tmp libs again

void arbet::arbet_merge_many_dp_free(const vector<string> & vec_in,
							  const string & lib_dir_new,
							  const string & lib_name_new) const
{
	int i;
	int tmp_int;

	tmp_int = vec_in.size();

	vector<string> tmp_vec(vec_in);

	// create tmp duplicated objects free static libs
	
	for (i = 0; i != tmp_int; i = i+3)
	{
		arbet_remove_duplicates(vec_in[i],
          	                   vec_in[i+1],
						    vec_in[i+2],
                    	         "free_tmp_" + vec_in[i+1]);
	}

	// update tmp_vec

	for(i = 0; i != tmp_int; i = i+3)
	{
		tmp_vec[i] = vec_in[i+2];
     	tmp_vec[i+1] = "free_tmp_" + vec_in[i+1];
     	tmp_vec[i+2] = vec_in[i+2];
	}

	// merge all dp free libs into a new one

	arbet_merge_many(tmp_vec,
				  lib_dir_new,
				  lib_name_new);

	// delete all the tmp created static libs
	
	for (i = 0; i != tmp_int; i = i+3)
	{
		arbet_delete_files(tmp_vec[i+2], all_a);
	}
}

} // end of namespace --> pgg

//======//
// FINI //
//======//

#endif // ARBET_DEF_H

