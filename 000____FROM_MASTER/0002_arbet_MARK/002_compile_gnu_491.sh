#!/bin/bash

  # 1. compile

  g++-4.9.1  -O3                \
             -Wall              \
             -std=gnu++14       \
             -static            \
             driver_program.cpp \
             -I/opt/boost/1560/gnu_491                               \
             /opt/boost/1560/gnu_491/stage/lib/libboost_system.a     \
             /opt/boost/1560/gnu_491/stage/lib/libboost_filesystem.a \
             -o x_gnu_491
