
//====================================//
// driver program for the arbet class //
//====================================//

// Note: find and list duplicated objects

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be sorted

     const string lib_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_name = "libmkl_core.a";
	
	// file new

	const string file_dir_out = base_dir + "/not_monitored/uniqued_files";
	const string file_name_out = "out_ar_t_4";

     // call the function

     arbet arbetObj;

     arbetObj.arbet_duplicated_objects(lib_dir,
                                       lib_name,
                                       file_dir_out,
							    file_name_out);

     // exit

     return 0;
}

//======//
// FINI //
//======//

