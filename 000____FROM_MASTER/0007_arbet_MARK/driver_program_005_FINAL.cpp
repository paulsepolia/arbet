
//====================================//
// driver program for the arbet class //
//====================================//

// Note: executes the "ar t" command to a static library

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // library

     const string lib_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_name = "libmkl_core.a";
	
	// file

	const string file_dir = base_dir + "/not_monitored/help_files";
	const string file_name = "out_ar_t_1";

     // call the function

     arbet arbetObj;

     arbetObj.arbet_create_file_ar_t(lib_dir,
                                     lib_name,
                                     file_dir,
							  file_name);

     // exit

     return 0;
}

//======//
// FINI //
//======//

