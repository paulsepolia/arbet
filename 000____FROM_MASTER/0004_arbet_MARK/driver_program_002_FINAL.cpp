
//====================================//
// driver program for the arbet class //
//====================================//

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::string;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     const string base_dir      = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";
     const string objects_a_dir = base_dir + "/not_monitored/libmkl_core_objects_a";
     const string lib_a_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_a_name    = "libmkl_core.a";
     const string objects_b_dir = base_dir + "/not_monitored/libmkl_core_objects_b";
     const string lib_b_dir     = base_dir + "/not_monitored/libs_original";
     const string lib_b_name    = "libmkl_core.a";
     const string lib_c_dir     = base_dir + "/not_monitored/libs_new";
     const string lib_c_name    = "libmkl_core_2.a";

     // call the function

     arbet arbetObj;

     arbetObj.arbet_merge_two(lib_a_dir,
                              lib_a_name,
                              objects_a_dir,
                              lib_b_dir,
                              lib_b_name,
                              objects_b_dir,
                              lib_c_dir,
                              lib_c_name);

     // exit

     return 0;
}

//======//
// FINI //
//======//

