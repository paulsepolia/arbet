
#ifndef ARBET_H
#define ARBET_H

//=============//
// class arbet //
//=============//

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace pgg {

class arbet {
public:

     //==================================//
     // No constructors - No destructors //
     //==================================//

     //==================//
     // member functions //
     //==================//

     // --> 1

     void arbet_extract(const string &,        // folder of the library
                        const string &,        // file name of the library
                        const string &) const; // folder of the objects to be written

     // --> 2

     void arbet_extract_create(const string &,        // folder of the original library
                               const string &,        // file name of the original library
                               const string &,        // folder of the objects to be written
                               const string &,        // folder of the new library
                               const string &) const; // file name of the new library

     // --> 3

     void arbet_merge_two(const string &,        // folder of the library A
                          const string &,        // file name of the library A
                          const string &,        // folder of the objects to be written
                          const string &,        // folder of the library B
                          const string &,        // file name of the library B
                          const string &,        // folder of the objects to be written
                          const string &,        // folder of the new library C
                          const string &) const; // name of the new library C

     // --> 4

     void arbet_extract_many(const vector<string> &) const; // vector to hold all the info for input libs

     // --> 5

     void arbet_merge_many(const vector<string> &, // vector to hpld all the info for input libs
                           const string &,         // folder of the new library C
                           const string &) const;  // name of the new library

private:

     // private member functions

     template <class T>
     void arbet_check_system(const T &) const;

     // commands

     const string ar_x  = "ar x";
     const string ar_r  = "ar r";
     const string mv_o  = " mv *.o";
     const string all_o = "*.o";

     // filesystem seperators

     const string space = " ";
     const string slash = "/";
};

} // end of namespace --> pgg

//======//
// FINI //
//======//

#endif // end of ARBET_H

