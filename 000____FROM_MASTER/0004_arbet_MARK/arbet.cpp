
#ifndef ARBET_DEF_H
#define ARBET_DEF_H

//========================//
// arbet class definition //
//========================//

#include "arbet.h"

// standard c++ library

#include <iostream>
#include <cstdlib>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::string;

// boost c++ library

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

namespace pgg {

//=========================================//
// arbet class member functions definition //
//=========================================//

// --> 1

template <>
void arbet::arbet_check_system(const int & tmp_int) const
{
     if (tmp_int == 0) {
          cout << " --> system command executed with success" << endl;
     } else {
          cout << " --> ERROR: system command did not execute!" << endl;
          cout << " --> Enter an integer to exit:";
          int sentinel;
          cin >> sentinel;
          exit(-1);
     }
}

// --> 2

template <>
void arbet::arbet_check_system(const bool & tmp_bool) const
{
     if (tmp_bool == true) {
          cout << " --> directory created with success" << endl;
     } else {
          cout << " --> directory already exits" << endl;
     }
}


// --> 3

// --> void arbet_extract(const string &,
//			  const string &,
//		          const string &) const

//=============================================//
// Extract object from a static library        //
// to spefified folder                         //
//=============================================//

//==================================================//
// Note: If there are objects with the same name    //
//       then are overwritten by the last extracted //
//========================================-=========//

void arbet::arbet_extract(const string & lib_orig_dir,
                          const string & lib_orig_name,
                          const string & objects_dir) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // create director for the object files

     tmp_bool = create_directories(objects_dir.c_str());

     // check

     this->arbet_check_system(tmp_bool);

     // extract the objects

     tmp_string = ar_x +
                  space +
                  lib_orig_dir + slash + lib_orig_name;

     tmp_int = system(tmp_string.c_str());

     // check

     this->arbet_check_system(tmp_int);

     // move the object to specific location

     tmp_string = mv_o +
                  space +
                  objects_dir;

     tmp_int = system(tmp_string.c_str());

     // check

     this->arbet_check_system(tmp_int);
}

// --> 4

// --> void arbet_extract_create(const string &,
//				 const string &,
//				 const string &,
//				 const string &,
//				 const string &) const

//=============================================//
// Extract object from a static library        //
// and archives them again to create a new one //
//=============================================//

//==========================================================//
// Note: The new static library must be the same            //
//       with the original one UNLESS there are             //
//       objects with the same name in the original library //
//       so during the extraction process the objects       //
//       with the same name overwrites each other           //
//       so we end up with one instance only                //
//       of those objects                                   //
//==========================================================//

void arbet::arbet_extract_create(const string & lib_orig_dir,
                                 const string & lib_orig_name,
                                 const string & objects_dir,
                                 const string & lib_new_dir,
                                 const string & lib_new_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects

     arbet_extract(lib_orig_dir,
                   lib_orig_name,
                   objects_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     this->arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  objects_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     this->arbet_check_system(tmp_int);

}

// --> 5

//=================================================//
// Note: extracts objects from two static libaries //
//       then builds a new one with those objects  //
//=================================================//

void arbet::arbet_merge_two(const string & lib_a_orig_dir,
                            const string & lib_a_orig_name,
                            const string & objects_a_dir,
                            const string & lib_b_orig_dir,
                            const string & lib_b_orig_name,
                            const string & objects_b_dir,
                            const string & lib_c_dir,
                            const string & lib_c_name) const
{
     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // exctract the objects of the first library

     arbet_extract(lib_a_orig_dir,
                   lib_a_orig_name,
                   objects_a_dir);

     // exctract the objects of the second library

     arbet_extract(lib_b_orig_dir,
                   lib_b_orig_name,
                   objects_b_dir);

     // create director for the new library

     tmp_bool = create_directories(lib_c_dir.c_str());

     // check

     this->arbet_check_system(tmp_bool);

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_c_dir + slash + lib_c_name +
                  space +
                  objects_a_dir + slash + all_o +
                  space +
                  objects_b_dir + slash + all_o;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     this->arbet_check_system(tmp_int);

}

// --> 6

//==================================================//
// Note: extracts objects from many static libaries //
//==================================================//

void arbet::arbet_extract_many(const vector<string> & vec_input) const

{
     // local variables

     int tmp_int;
     int i;

     // get vector's length

     tmp_int = vec_input.size();

     // test if it is ok

     if ( ((tmp_int >=3) && (tmp_int%3 != 0)) || (tmp_int < 3)) {
          cout << " --> ERROR: input vector's size is wrong!" << endl;
          cout << " --> Enter an integer to exit: ";
          int sentinel;
          cin >> sentinel;
     }

     // extract all the objects

     for (i = 0; i < tmp_int; i = i+3) {
          this->arbet_extract(vec_input[i],
                              vec_input[i+1],
                              vec_input[i+2]);
     }
}

// --> 7

//==================================================//
// Note: extracts objects from many static libaries //
//       then builds a new one with those objects   //
//==================================================//

void arbet::arbet_merge_many(const vector<string> & vec_input,
                             const string & lib_new_dir,
                             const string & lib_new_name) const

{
     // local variables

     bool tmp_bool;
     string tmp_string;
     int i;
     int tmp_int;

     // extract all the objects

     this->arbet_extract_many(vec_input);

     // create director for the new library

     tmp_bool = create_directories(lib_new_dir.c_str());

     // check

     this->arbet_check_system(tmp_bool);

     // build string with all the input arguments to ar command

     tmp_string = "";

     for (i = 0; i < vec_input.size(); i = i+3) {
          tmp_string = tmp_string +
                       vec_input[i+2] + slash + all_o +
                       space;
     }

     // build the new library

     tmp_string = ar_r +
                  space +
                  lib_new_dir + slash + lib_new_name +
                  space +
                  tmp_string;

     cout << " --> building the new static library ... please wait!" << endl;

     tmp_int = system(tmp_string.c_str());

     // check

     this->arbet_check_system(tmp_int);
}

} // end of namespace --> pgg

//======//
// FINI //
//======//

#endif // ARBET_DEF_H

