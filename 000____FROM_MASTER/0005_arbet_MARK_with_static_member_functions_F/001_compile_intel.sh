#!/bin/bash

  # 1. compile

  icpc -O3                \
       -xHost             \
       -Wall              \
       -std=c++11         \
       -static            \
       -wd2012            \
       -openmp            \
       driver_program.cpp \
       -I/opt/boost/1560/intel                               \
       /opt/boost/1560/intel/stage/lib/libboost_system.a     \
       /opt/boost/1560/intel/stage/lib/libboost_filesystem.a \
       -o x_intel

