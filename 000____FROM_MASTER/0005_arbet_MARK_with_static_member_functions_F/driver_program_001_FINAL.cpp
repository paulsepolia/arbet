
//====================================//
// driver program for the arbet class //
//====================================//

// Note: extract objects from a static library
//       and then builds a new static library from those
//       Same objects are overwritten during extraction

#include "arbet_def.h"
#include "arbet_decl.h"


#include <iostream>
#include <cstdlib>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::string;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     const string base_dir      = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";
     const string objects_dir   = base_dir + "/not_monitored/libmkl_core_objects";
     const string lib_orig_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_orig_name = "libmkl_core.a";
     const string lib_new_dir   = base_dir + "/not_monitored/libs_new";
     const string lib_new_name  = "libmkl_core_new.a";

     // call the function


     arbet::arbet_extract_create(lib_orig_dir,
                                 lib_orig_name,
                                 objects_dir,
                                 lib_new_dir,
                                 lib_new_name);

     // exit

     return 0;
}

//======//
// FINI //
//======//

