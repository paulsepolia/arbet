#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=c++0x         \
       -static            \
	  -fopenmp           \
       driver_program.cpp \
       -I/opt/boost/1560/gnu_ubu                               \
       /opt/boost/1560/gnu_ubu/stage/lib/libboost_system.a     \
       /opt/boost/1560/gnu_ubu/stage/lib/libboost_filesystem.a \
       -o x_gnu_ubu
