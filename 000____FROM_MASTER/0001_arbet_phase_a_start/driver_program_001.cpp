
//=============================================//
// Extract object from a static library        //
// and archives them again to create a new one //
//=============================================//

//==========================================================//
// Note: The new static library must be the same            //
//       with the original one UNLESS there are             //
//       objects with the same name in the original library //
//       so during the extraction process the objects       //
//       with the same name overwrites each other           //
//       so we end up with one instance only                //
//       of those objects                                   //
//==========================================================//

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;
using std::to_string;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

// the main function

int main()
{
     // local parameters
    
     // directories and file names

     const string base_dir      = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";
     const string objects_dir   = base_dir + "/not_monitored/libmkl_core_objects";
     const string lib_orig_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_orig_name = "libmkl_core.a";
     const string lib_new_dir   = base_dir + "/not_monitored/libs_new";
     const string lib_new_name  = "libmkl_core_new.a";
     const string ar_x  = "ar x";

     // commands

     const string ar_r  = "ar r";
     const string mv_o  = " mv *.o";
     const string all_o = "*.o";
 
     // filesystem seperators

     const string space = " ";
     const string slash = "/";

     // local variables

     int tmp_int;
     bool tmp_bool;
     string tmp_string = "";

     // adjust the output format

     cout << boolalpha;

     // create director for the object files

     cout << " --> 001 --> create directory for the objects" << endl;

     tmp_bool = create_directories(objects_dir.c_str());

     cout << " --> report --> " << tmp_bool << endl;

     // extract the objects

     cout << " --> 002 --> extract the objects" << endl;

     tmp_string = ar_x + space + lib_orig_dir + slash + lib_orig_name;

     tmp_int = system(tmp_string.c_str());

     cout << " --> report --> " << tmp_int << endl;

     // move the object to specific location

     cout << " --> 003 --> move the objects to their location" << endl;

     tmp_string = mv_o + space + objects_dir;

     tmp_int = system(tmp_string.c_str());

     cout << " --> report --> " << tmp_int << endl;

     // create director for the new library

     cout << " --> 004 --> create directory for the new library" << endl;

     tmp_bool = create_directories(lib_new_dir.c_str());

     cout << " --> report --> " << tmp_bool << endl;

     // build the new library

     cout << " --> 005 --> build the new library" << endl;

     tmp_string = objects_dir + slash + all_o;

     tmp_string = ar_r + space +
                  lib_new_dir + slash + lib_new_name + space +
                  objects_dir + slash + all_o;

     tmp_int = system(tmp_string.c_str());

     cout << " --> report --> " << tmp_int << endl;

     return 0;
}

//======//
// FINI //
//======//

