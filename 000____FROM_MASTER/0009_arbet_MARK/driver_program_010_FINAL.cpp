
//====================================//
// driver program for the arbet class //
//====================================//

// Note: find and list duplicated objects

#include "arbet.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::boolalpha;

#include <boost/filesystem.hpp>

using boost::filesystem::create_directories;

using pgg::arbet;

// the main function

int main()
{
     // local parameters

     // base directory

     const string base_dir = "/home/pgg/pgg/bazaar_projects/not_finished/arbet";

     // file to be sorted

     const string lib_dir  = base_dir + "/not_monitored/libs_original";
     const string lib_name = "libmkl_core.a";
	
	// file new

	const string file_dir_out = base_dir + "/not_monitored/uniqued_files";
	const string file_name_out = "out_ar_t_4";

	// local variables

	arbet arbetObj;
	bool tmp_bool;

     // produce list of duplicated objects

     arbetObj.arbet_duplicated_objects(lib_dir,
                                       lib_name,
                                       file_dir_out,
							    file_name_out);

	// check if the list is empty
	// so check if the static libary has dublicates

	tmp_bool = !arbetObj.arbet_is_file_empty(file_dir_out,
                                              file_name_out);

	cout << boolalpha;

	cout << " --> static library has dublicates --> " << tmp_bool << endl;

     // exit

     return 0;
}

//======//
// FINI //
//======//

